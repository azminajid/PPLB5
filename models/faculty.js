var db = require('../db');

exports.getAll = function(callback) {
    var fakultas = db.get().collection('Faculty');
    fakultas.find({}).toArray(function (err, items) {
        callback(err, items);
    });
}
