var db = require('../db')

// get all possible interest
exports.getAll = function(callback) {
    var status = db.get().collection('DummyInterest');
    status.find({}).toArray(function (err, items) {
        callback(err, items);
    });
}
