var db = require('../db')

// get all possible status
exports.getAll = function(callback) {
    var status = db.get().collection('DummyStatus');
    status.find({}).toArray(function (err, items) {
        callback(err, items);
    });
}