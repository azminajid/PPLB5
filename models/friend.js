var db = require('../db');
var user = require('./user');

// get Friends
exports.get = function(username, callback) {
	var profile = db.get().collection('DummyProfile2');
	var friends_arr = [];
	profile.findOne({"username":username}, function (err, result) {
		if(err){
			callback(1,"not found");
		}

		friends_arr = result.friend_id;
		if(friends_arr.length != 0){
			profile.aggregate([
				{$match: {id:  { $in: friends_arr} }},
				{$lookup: {from: 'DummyStatus' ,localField: 'status', foreignField: 'id', as:'status' }}
				], function(err, items) {

					callback(err,{friends: items});
				});
		} else {
			callback(err,"empty");
		}    
	});
}

// get friend request
exports.getRequest = function(requested, callback) {
	var request = db.get().collection('DummyRequest');
	request.aggregate([
		{$match: {requested:  requested }},
		{$lookup: {from: 'DummyProfile2' ,localField: 'requester', foreignField: 'username', as:'requesterProfile' }}
		], function(err, items) {
			if(err) {
				callback(err, "Internal Server Error");
			} 
			callback(err, items);
		});
}

// delete friend request to decline request
exports.deleteFriendRequest = function(requested, requester, callback) {
	var request = db.get().collection('DummyRequest');
	request.deleteOne({requested: requested, requester: requester}, function(err, result){
		callback(err);
	});
}

// add requested to requester friends and vice versa 
exports.addToFriend = function(requested, requester, callback) {
	var profile = db.get().collection('DummyProfile2');
	user.get(requested, function(err, requestedProfile){
		if(err) {
			callback(err);
		} else {
			user.get(requester, function(err, requesterProfile) {
				if(err) {
					callback(err);
				} else {
					var requested_new_friend_id = requestedProfile.user.friend_id;
					requested_new_friend_id.push(requesterProfile.user.id);
					var setter_requested = {
						friend_id: requested_new_friend_id
					}
					var requester_new_friend_id = requesterProfile.user.friend_id;
					requester_new_friend_id.push(requestedProfile.user.id);
					var setter_requester = {
						friend_id: requester_new_friend_id
					}
					profile.update({username:requested}, {$set: setter_requested}, function(err,result){
						profile.update({username:requester}, {$set: setter_requester}, function(err,result){
							callback(err);
						});
					});
				}
			});
		}
	});
}

//show notification
exports.getTotalRequest = function(username, callback) {
	var profile = db.get().collection('DummyRequest');
	profile.find({"requested":username}, function (err, result) {
		if(err){
			callback(1,"not found");
		}else {
			result.count(function(err,result){
				callback(err,result);
			});
		}
	});
}


