
exports.signed = function (req, res, next) {
    if(req.session.authenticated && req.session.username == req.params.username) {
        next();
    } else {
        res.status(403);
        res.send('403: forbidden');
    }
}

exports.valid = function (req, res, next) {
    if(req.session.authenticated) {
        next();
    } else {
        res.status(403);
        res.send('403: forbidden');
    }
}
