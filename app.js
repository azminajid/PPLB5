var express = require('express');
var app = express();

// set view engine
var hbs =  require('express-handlebars');
app.engine('.hbs', hbs({extname: '.hbs', defaultLayout:'main', layoutsDir: __dirname + '/views/layouts/', partialsDir:__dirname + '/views/partials/'}));
app.set('view engine', 'hbs');

// use express session
var cookieParser = require('cookie-parser');
var session = require('express-session');
app.use(cookieParser());
app.use(session({ 
    secret: 'umeeti',
    resave: false,
    saveUninitialized: true,
    cookie: { secure: false } 
}));

// form validate
var bodyParser = require("body-parser");
var validator = require('express-validator');
var validator_helper = require("./helpers/validators");
app.use(bodyParser.urlencoded({ extended: false }));
app.use(validator({ customValidators: validator_helper.functions }));

// set directory
app.use("/", express.static(__dirname + '/public'));
app.use(require('./controllers/routing')); 

// set database
var db = require('./db');
var db_url = "mongodb://helum3:wisnuganteng666@ds113650.mlab.com:13650/umeeti";
db.connect(db_url, function(err) {
    if (err) {
        console.log('Unable to connect to Mongo.')
        process.exit(1)
    } else {
        app.listen(process.env.PORT||5000, function() {
            console.log('Listening on port 5000...');
        });
    }
})

