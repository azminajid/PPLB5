var chai = require('chai');
var assert = chai.assert;
var expect = chai.expect;
var should = chai.should();
var sinon = require('sinon');

var validatorjs = require('../../helpers/validators');
var isLink = validatorjs.functions.isLink;

describe('helpers/validators.js', function(){

    it('isLink should return true if link is started with https://params', function(){
        var link = "https://www.facebok.com/azmi";
        var param = "www.facebok.com/";
        var return_value = isLink(link, param);
        expect(return_value).to.be.true;
    });

    it('isLink should return false if link is not started with https://', function(){
        var link = "www.facebok.com/azmi";
        var param = "www.facebok.com/";
        var return_value = isLink(link, param);
        expect(return_value).to.be.false;
    });

    it('isLink should return false if link is started with https:// but without params', function(){
        var link = "https://www.azmi.com/";
        var param = "www.facebok.com/";
        var return_value = isLink(link, param);
        expect(return_value).to.be.false;
    });
});

