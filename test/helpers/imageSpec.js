var chai = require('chai');
var assert = chai.assert;
var expect = chai.expect;
var should = chai.should();
var sinon = require('sinon');

var imagejs = require('../../helpers/image');
var util = require("util");
var mime = require("mime");

describe('helpers/image.js', function(){
  it('should return true to validate the mime_file for jpeg, png or jpg', function(){
    var mime_file = "image/jpeg";
    var value = imagejs.validation(mime_file);
    assert.equal(value,true);

    var mime_file = "image/png";
    var value = imagejs.validation(mime_file);
    assert.equal(value,true);

    var mime_file = "image/jpg";
    var value = imagejs.validation(mime_file);
    assert.equal(value,true);
  });

  it('should return false to validate the mime_file is neither jpeg, png nor jpg', function(){
    var mime_file = "only/image";
    var value = imagejs.validation(mime_file);
    assert.equal(value,false);
  });

  // it('should return base64image', function(){
  //   var src = "testingSrc";
  //   var data = "test";
  //   var expected = util.format("data:%s;base64,%s", mime.lookup(src), data);
  //   var value = imagejs.base64Image(src);
  //   assert.equal(value,expected);
  // });
});
