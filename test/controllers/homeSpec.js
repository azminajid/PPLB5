var chai = require('chai');
var assert = chai.assert;
var should = chai.should();
var sinon = require('sinon');

var homejs = require('../../controllers/home');

describe('controllers/home.js', function(){
  it('should render the page', function(){
    var req, res, spy;
    req = res = {};
    renderSpy = res.render = sinon.spy();
    homejs.index(req,res);
    renderSpy.withArgs("home", {title: "umeeti", layout: "home"}).calledOnce.should.be.true;
  });
});
