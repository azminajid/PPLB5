var chai = require('chai');
var assert = chai.assert;
var expect = chai.expect;
var should = chai.should();
var sinon = require('sinon');

var auth = require('../../controllers/auth');
var user = require('../../models/user');

describe('controllers/auth.js', function(){

    describe('#viewLogin()', function(){
        
        it('should render the page', function(){
            var req, res;
            req = res = {};
            renderSpy = res.render = sinon.spy();
            auth.viewLogin(req,res);
            renderSpy.withArgs("login", {title: "umeeti"}).calledOnce.should.be.true;
        });

    });

    describe('#submitLogin()', function(){
        
        it('should redirect to profile page if login details are correct', function(){
            var req = {
                body: {
                    username: 'Carl.Brown'
                }, 
                session: {
                    authenticated: false,
                    username: null
                }
            }
            var res = {
                redirect: sinon.spy()
            }
            var profile = sinon.stub(user,'get').yields(0, {user: 'Carl.Brown'});
            auth.submitLogin(req,res);
            res.redirect.calledOnce.should.be.true;
            user.get.restore();
            expect(res.redirect.args[0][0]).equal('/profile/Carl.Brown');
            expect(req.session.authenticated).to.be.true;
            expect(req.session.username).equal('Carl.Brown');
        });

        it('should redirect to login page if login details are not correct',function(){
            var req = {
                body: {
                    username: 'Carl.Brown'
                }, 
                session: {
                    authenticated: null,
                    username: null
                }
            }
            var res = {
                redirect: sinon.spy()
            }
            var profile = sinon.stub(user,'get').yields(1, null);
            auth.submitLogin(req,res);
            res.redirect.calledOnce.should.be.true;
            user.get.restore();
            expect(res.redirect.args[0][0]).equal('/login');
            expect(req.session.authenticated).to.be.null;
            expect(req.session.username).to.be.null;
        });

    });

    describe('#logout()', function() {
        
        it('should remove session',function(){
            var req = {
                body: {
                    username: 'Carl.Brown'
                }, 
                session: {
                    authenticated: null,
                    username: null
                }
            }
            var res = {
                redirect: sinon.spy()
            }
            auth.logout(req,res);
            expect(req.session.authenticated).to.be.undefined;
            expect(req.session.username).to.be.undefined;
        });
    });

});

