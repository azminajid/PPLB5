var chai = require('chai');
var assert = chai.assert;
var expect = chai.expect;
var should = chai.should();
var sinon = require('sinon');

var userjs = require('../../models/user');
var db = require('../../db');

describe('models/user.js', function(){
  var details, profile, callback, callbackSpy;
  beforeEach(function() {
    details = {
      findOne: function(username){
        return true;
      },
      update: function(){
        return true;
      },
      aggregate: function(){

      },
      find : function(err, result) {
        return result;
      }
    };
    profile = {
      collection : function(myDb){
        return details;
      }
    };
    callbackSpy = sinon.spy(callback);
  });

  it('should return the user details based on username', function(){
    var myProf = sinon.stub(db,'get').returns(profile);
    var findStub = sinon.stub(details,'findOne').yields(3,"Carl");
    userjs.get("Carl", callbackSpy);
    callbackSpy.withArgs(3,{user : "Carl"}).calledOnce.should.be.true;
    details.findOne.restore();
  });

  it('should return callback not found if the username is not found on database', function(){
    findStub = sinon.stub(details,'findOne').yields(3, null);
    userjs.get("Carl", callbackSpy);
    callbackSpy.withArgs(1,"not found").calledOnce.should.be.true;
    details.findOne.restore();
  });

 it('should update the user status', function(){
    var user_update = {
        email:"Carl@example.com",
        phone: "08123232123",
        location: "USA",
        skill: "javascript",
        achievement: "gemastik championship",
        interest: "programming",
        facebook: "facebook",
        twitter: "twitter",
        linkedin: "linkedin",
        photo: "photo",
        status: "status"
    }
    var setter = user_update;
    setter.photo = user_update.photo;
    setter.status = user_update.status;

    findStub = sinon.stub(details,'update').yields(3, "Carl");
    userjs.update("Carl", user_update, callbackSpy);
    callbackSpy.withArgs(3).calledOnce.should.be.true;
    details.update.restore();
  });

  it('should get the user with status', function(){
    var mystatus = {
      status : "Available"
    };
    var users = {
      status : [
        mystatus
      ]
    };

   
    var items = [users,"email"];
    findStub = sinon.stub(details,'aggregate').yields(3, items);
    userjs.getWithStatus("Carl", callbackSpy);
    callbackSpy.withArgs(3,{user : users, status : "Available"}).called.should.be.true;
    details.aggregate.restore();
  });

  it('should error in get the user with status', function(){
    var mystatus = {
      status : "Available"
    };
    var users = {
      status : [
        mystatus
      ]
    };

    var err = new Error("User not found");
    var items = [];
    var ret = {};

    findStub = sinon.stub(details,'aggregate').yields(err, items);
    userjs.getWithStatus("Carl.Brown", callbackSpy);
    callbackSpy.called.should.be.true;
    details.aggregate.restore();
  });

  it('should return error when find all faculty and interest not found', function() {
    var obj = {
      toArray : function(err, obj){}
    }
    sinon.stub(details, 'find').returns(obj);
    findStub = sinon.stub(obj, 'toArray').yields(1, "");
    userjs.getNewFriends("All", "All", "Muhammad.Izzan", callbackSpy);
    callbackSpy.withArgs(1, "not found").calledOnce.should.be.true;
    details.find.restore();
  }); 


  it('should return error when find all faculty and interest not found', function() {
    var obj = {
      toArray : function(err, obj){}
    }
    sinon.stub(details, 'find').returns(obj);
    findStub = sinon.stub(obj, 'toArray').yields(1, "");
    userjs.getNewFriends("All", "All", "Muhammad.Izzan", callbackSpy);
    callbackSpy.withArgs(1, "not found").calledOnce.should.be.true;
    details.find.restore();
  }); 

  it('should return all users when user search faculty and interest in all options', function() {
    var obj = {
      toArray : function(err, obj){}
    }
    var result = {
      friend_id : {
        push : function() {}
      },
      id : "7"
    }
    
    sinon.stub(details, 'find').returns(obj);
    findStub = sinon.stub(obj, 'toArray').yields(false, result);
    sinon.stub(details, 'findOne').yields(1, result);
    sinon.stub(details, 'aggregate').yields(3, "item");
    userjs.getNewFriends("All", "All", "Muhammad.Izzan", callbackSpy);

    callbackSpy.withArgs(3, {user: "item"}).calledOnce.should.be.true;
    details.find.restore();
  });

  it('should return error when user search faculty and interest in all options', function() {
    var obj = {
      toArray : function(err, obj){}
    }
    var result = {
      friend_id : {
        push : function() {}
      },
      id : "7"
    }
    sinon.stub(details, 'find').returns(obj);
    findStub = sinon.stub(obj, 'toArray').yields(false, result);
    sinon.stub(details, 'findOne').yields(true, "");
    sinon.stub(details, 'aggregate').yields(3, "item");
    userjs.getNewFriends("All", "All", "Muhammad.Izzan", callbackSpy);

    callbackSpy.withArgs(1, "not found").calledOnce.should.be.true;
    details.find.restore();
  });

  it('should return error when interest that chosen by user is All', function() {
    var obj = {
      toArray : function(err, obj){}
    }
    sinon.stub(details, 'find').returns(obj);
    findStub = sinon.stub(obj, 'toArray').yields(1, "");
    userjs.getNewFriends("All", "Fakultas Ilmu Komputer", "Muhammad.Izzan", callbackSpy);
    callbackSpy.withArgs(1, "not found").calledOnce.should.be.true;
    details.find.restore();
  });

  it('should return all users when user search for All interest but specified faculty', function() {
    var obj = {
      toArray : function(err, obj){}
    }
    var result = {
      friend_id : {
        push : function() {}
      },
      id : "5"
    }
    sinon.stub(details, 'find').returns(obj);
    findStub = sinon.stub(obj, 'toArray').yields(false, result);
    sinon.stub(details, 'findOne').yields(1, result);
    sinon.stub(details, 'aggregate').yields(3, "item");
    userjs.getNewFriends("All", "Fakultas Ilmu Komputer", "Muhammad.Izzan", callbackSpy);

    callbackSpy.withArgs(3, {user: "item"}).calledOnce.should.be.true;
    details.find.restore();
  });

  it('should return error when user search for All interest but specified faculty', function() {
    var obj = {
      toArray : function(err, obj){}
    }
    var result = {
      friend_id : {
        push : function() {}
      },
      id : "5"
    }
    sinon.stub(details, 'find').returns(obj);
    findStub = sinon.stub(obj, 'toArray').yields(false, result);
    sinon.stub(details, 'findOne').yields(true, "");
    sinon.stub(details, 'aggregate').yields(3, "item");
    userjs.getNewFriends("All", "Fakultas Ilmu Komputer", "Muhammad.Izzan", callbackSpy);

    callbackSpy.withArgs(1, "not found").calledOnce.should.be.true;
    details.find.restore();
  });

  it('should return error when faculty that chosen by user is All', function() {
    var obj = {
      toArray : function(err, obj){}
    }
    sinon.stub(details, 'find').returns(obj);
    findStub = sinon.stub(obj, 'toArray').yields(1, "");
    userjs.getNewFriends("programming", "All", "Muhammad.Izzan", callbackSpy);
    callbackSpy.withArgs(1, "not found").calledOnce.should.be.true;
    details.find.restore();
  });

  it('should return all users when user search for All faculty but specified interest', function() {
    var obj = {
      toArray : function(err, obj){}
    }
    var result = {
      friend_id : {
        push : function() {}
      },
      id : "5"
    }
    sinon.stub(details, 'find').returns(obj);
    findStub = sinon.stub(obj, 'toArray').yields(false, result);
    sinon.stub(details, 'findOne').yields(1, result);
    sinon.stub(details, 'aggregate').yields(3, "item");
    userjs.getNewFriends("programming", "All", "Muhammad.Izzan", callbackSpy);

    callbackSpy.withArgs(3, {user: "item"}).calledOnce.should.be.true;
    details.find.restore();
  });

  it('should return error when user search for All faculty but specified interest', function() {
    var obj = {
      toArray : function(err, obj){}
    }
    var result = {
      friend_id : {
        push : function() {}
      },
      id : "5"
    }
    sinon.stub(details, 'find').returns(obj);
    findStub = sinon.stub(obj, 'toArray').yields(false, result);
    sinon.stub(details, 'findOne').yields(true, "");
    sinon.stub(details, 'aggregate').yields(3, "item");
    userjs.getNewFriends("programming", "All", "Muhammad.Izzan", callbackSpy);

    callbackSpy.withArgs(1, "not found").calledOnce.should.be.true;
    details.find.restore();
  });

  it('should return error when user search for specified faculty and interest', function() {
    var obj = {
      toArray : function(err, obj){}
    }
    sinon.stub(details, 'find').returns(obj);
    findStub = sinon.stub(obj, 'toArray').yields(1, "");
    userjs.getNewFriends("programming", "Fakultas Ilmu Komputer", "Muhammad.Izzan", callbackSpy);
    callbackSpy.withArgs(1, "not found").calledOnce.should.be.true;
    details.find.restore();
  })

  it('should return search result for specified faculty and interest', function() {
    var obj = {
      toArray : function(err, obj){}
    }
    var result = {
      friend_id : {
        push : function() {}
      },
      id : "5"
    }
    sinon.stub(details, 'find').returns(obj);
    findStub = sinon.stub(obj, 'toArray').yields(false, result);
    sinon.stub(details, 'findOne').yields(1, result);
    sinon.stub(details, 'aggregate').yields(3, "item");
    userjs.getNewFriends("programming", "Fakultas Ilmu Komputer", "Muhammad.Izzan", callbackSpy);

    callbackSpy.withArgs(3, {user: "item"}).calledOnce.should.be.true;
    details.find.restore();
  })

   it('should return error when search for specified faculty and interest', function() {
    var obj = {
      toArray : function(err, obj){}
    }
    var result = {
      friend_id : {
        push : function() {}
      },
      id : "5"
    }
    sinon.stub(details, 'find').returns(obj);
    findStub = sinon.stub(obj, 'toArray').yields(false, result);
    sinon.stub(details, 'findOne').yields(true, "");
    sinon.stub(details, 'aggregate').yields(3, "item");
    userjs.getNewFriends("programming", "Fakultas Ilmu Komputer", "Muhammad.Izzan", callbackSpy);

    callbackSpy.withArgs(1, "not found").calledOnce.should.be.true;
    details.find.restore();
  })
});
