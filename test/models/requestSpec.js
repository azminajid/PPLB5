var chai = require('chai');
var should = chai.should();
var sinon = require('sinon');

var requestjs = require('../../models/request.js');
var insert = require('../../models/insertRequest.js');

var db = require('../../db');

// Test for validation in request friend.
describe('models/requestFriend.js', function(){
  var details, request, callback, callbackSpy;
  
  beforeEach(function() {
    details = {
      findOne: function(){  
      },
	  insertOne: function(){
      }
    };
    request = {
      collection : function(myDb){
        return details;
      }
    };
    callbackSpy = sinon.spy(callback);
  });
  
  after(function () {
    db.get.restore();
  });

  
  it('should add if username have not created the request on database before', function(){
	sinon.stub(db,'get').returns(request);
    var req_update = {
	  requester: "Julie",
      requested: "Carl",
      message: "hello"
    };
	
	findStub = sinon.stub(details,'findOne').yields(3, false);
	insertStub = sinon.stub(details, 'insertOne').yields(3, req_update);
    requestjs.add("Julie", req_update, callbackSpy);
	callbackSpy.withArgs(3, 'hello').called.should.be.true;
	details.findOne.restore();
	details.insertOne.restore();

  });
  
  it('should return callback -request have been created- if username have created the request on database before', function(){
    var req_update = {
	  requester: "Julie",
      requested: "Carl",
      message: "hello"
    };
	
	findStub = sinon.stub(details,'findOne').yields(3, true);
    requestjs.add("Julie", req_update, callbackSpy);
	callbackSpy.withArgs(3, 'Request have been created').called.should.be.true;
    details.findOne.restore();

  });
  
});