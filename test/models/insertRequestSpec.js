var chai = require('chai');
var should = chai.should();
var sinon = require('sinon');

var requestjs = require('../../models/insertRequest');
var db = require('../../db');

// Test for insert request.
describe('models/insertRequest.js', function(){
  var details, request, callback, callbackSpy;
  
  beforeEach(function() {
    details = {
      insertOne: function(){
        return true;
      }
    };
    request = {
      collection : function(myDb){
        return details;
      }
    };
    callbackSpy = sinon.spy(callback);
  });
  
  after(function () {
    db.get.restore();
  });

  it('should update the request database', function(){
	sinon.stub(db,'get').returns(request);
    var req_update = {
	  requester: "Julie",
      requested: "Carl",
      message: "Hello"
    };
    findStub = sinon.stub(details,'insertOne').yields(3, "Carl");
    requestjs.add("Carl", req_update, callbackSpy);
    callbackSpy.withArgs(3, "Hello").called.should.be.true;
    details.insertOne.restore();
  });
  
});