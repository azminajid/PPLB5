var chai = require('chai');
var assert = chai.assert;
var should = chai.should();
var sinon = require('sinon');

var friendjs = require('../../models/friend');
var userjs = require('../../models/user');
var db = require('../../db');

describe('models/friend.js', function(){

  describe('#get()',function(){
    var details, profile, callback, callbackSpy, result, err, username, req, items;
    beforeEach(function(){
      result = { friend_id: [1]};
      username = {"username": "agni"};
      req = { params: username };
      details = {
        findOne : function(err,result){
          return result;
        },
        aggregate : function(err,items){
          return items
        }
      };
      profile = {
        collection : function(myDb){
          return details;
        },
      };
      callbackSpy = sinon.spy(callback);
    });

    it('should return friends', function(){
      err = null;
      var friends_arr = result.friend_id.length;
      var getStub = sinon.stub(db,'get').returns(profile);
      var findStub = sinon.stub(details,'findOne');
      findStub.yields(null,result);
      var aggregateStub = sinon.stub(details,'aggregate');
      aggregateStub.yields(err,items);
      friendjs.get(username,callbackSpy);
      callbackSpy.withArgs(null,{friends: items}).calledOnce.should.be.true;
      getStub.restore();
      findStub.restore();
      aggregateStub.restore();
    });

    it('should zero friend', function(){
      err = 1;
      result = {friend_id: []};
      var friends_arr = result.friend_id.length;
      var getStub = sinon.stub(db,'get').returns(profile);
      var findStub = sinon.stub(details,'findOne');
      findStub.yields(null,result);
      var aggregateStub = sinon.stub(details,'aggregate');
      aggregateStub.yields(err,items);  
      friendjs.get(username,callbackSpy);
      callbackSpy.withArgs(null,"empty").calledOnce.should.be.true;
      getStub.restore();
      findStub.restore();
      aggregateStub.restore();
    });

    it('should error find one', function(){
      err = "1";
      var getStub = sinon.stub(db,'get').returns(profile);
      var findStub = sinon.stub(details,'findOne');
      findStub.yields(err,result)
      findStub.returns(err,result);
      friendjs.get(username,callbackSpy);
      callbackSpy.withArgs(1,'not found').calledOnce.should.be.true;
      getStub.restore();
      findStub.restore();
    });
  });
  
  describe('#getRequest()', function(){
    var fakeDb, functions;
    beforeEach(function(){
      // pas db.get kita akan stub biar balikin fakeDb
      // pada getRequest asli akan menjalankan fungsi dari objek functions
      fakeDb = {
        collection: function(col){
          return functions;
        }
      };
      // object functionsnya gak diimplementasi, biar di stub di tesing aja
      // karena kita maunya beda-beda callbackDb
      functions = {
        aggregate: function(query, callbackDb) {}
      }
    });

    it('should return items from requester', function(){
      var dbStub = sinon.stub(db,'get').returns(fakeDb);
      var aggregateStub = sinon.stub(functions,'aggregate').yields(false,{});
      var callback = sinon.spy();
      friendjs.getRequest('azmi41',callback);
      callback.withArgs(false,{}).called.should.be.true;
      db.get.restore();
      functions.aggregate.restore();
    });

    it('should return error message if query error',function(){
      var dbStub = sinon.stub(db,'get').returns(fakeDb);
      var aggregateStub = sinon.stub(functions,'aggregate').yields(true,null);
      var callback = sinon.spy();
      friendjs.getRequest('azmi41',callback);
      callback.withArgs(true,'Internal Server Error').called.should.be.true;
      db.get.restore();
      functions.aggregate.restore();
    });
  });

  describe('#deleteFriendRequest()', function(){
    var fakeDb, functions;
    beforeEach(function(){
      // pas db.get kita akan stub biar balikin fakeDb
      // pada getRequest asli akan menjalankan fungsi dari objek functions
      fakeDb = {
        collection: function(col){
          return functions;
        }
      };
      // object functionsnya gak diimplementasi, biar di stub di tesing aja
      // karena kita maunya beda-beda callbackDb
      functions = {
        deleteOne: function(query, callbackDb) {}
      }
    });

    it('should return message if there is any error', function(){
      var dbStub = sinon.stub(db,'get').returns(fakeDb);
      var deleteOneStub = sinon.stub(functions,'deleteOne').yields('message');
      var callback = sinon.spy();
      friendjs.deleteFriendRequest('azmi41', 'akmal61', callback);
      callback.withArgs('message').called.should.be.true;
      db.get.restore();
      functions.deleteOne.restore();
    });
  });

  describe('#addToFriend()', function(){
    var fakeDb, functions;
    beforeEach(function(){
      // pas db.get kita akan stub biar balikin fakeDb
      // pada getRequest asli akan menjalankan fungsi dari objek functions
      fakeDb = {
        collection: function(col){
          return functions;
        }
      };
      // object functionsnya gak diimplementasi, biar di stub di tesing aja
      // karena kita maunya beda-beda callbackDb
      functions = {
        update: function(query, callbackDb) {}
      }
    });

    it('should return message if get requested user error', function(){
      var dbStub = sinon.stub(db,'get').returns(fakeDb);
      var userStub = sinon.stub(userjs,'get');
      userStub.withArgs('requested').yields('error requested', {user:{id:1,friend_id:[]}});
      userStub.withArgs('requester').yields(false, {user:{id:2,friend_id:[]}});
      var deleteOneStub = sinon.stub(functions,'update').yields('update message');
      var callback = sinon.spy();
      friendjs.addToFriend('requested','requester',callback);
      callback.withArgs('error requested').calledOnce.should.be.true;
      db.get.restore();
      userjs.get.restore();
      functions.update.restore();
    }); 

    it('should return message if get requester user error', function(){
      var dbStub = sinon.stub(db,'get').returns(fakeDb);
      var userStub = sinon.stub(userjs,'get');
      userStub.withArgs('requested').yields(false, {user:{id:1,friend_id:[]}});
      userStub.withArgs('requester').yields('error requester', {user:{id:2,friend_id:[]}});
      var deleteOneStub = sinon.stub(functions,'update').yields('update message');
      var callback = sinon.spy();
      friendjs.addToFriend('requested','requester',callback);
      callback.withArgs('error requester').calledOnce.should.be.true;
      db.get.restore();
      userjs.get.restore();
      functions.update.restore();
    });

    it('should return message from updating profile table', function(){
      var dbStub = sinon.stub(db,'get').returns(fakeDb);
      var userStub = sinon.stub(userjs,'get');
      userStub.withArgs('requested').yields(false, {user:{id:1,friend_id:[]}});
      userStub.withArgs('requester').yields(false, {user:{id:2,friend_id:[]}});
      var deleteOneStub = sinon.stub(functions,'update').yields('update message');
      var callback = sinon.spy();
      friendjs.addToFriend('requested','requester',callback);
      callback.withArgs('update message').calledOnce.should.be.true;
      db.get.restore();
      userjs.get.restore();
      functions.update.restore();
    });  

  });

   describe('#getTotalRequest()', function(){
    var fakeDb, functions;
    beforeEach(function(){
      // pas db.get kita akan stub biar balikin fakeDb
      // pada getRequest asli akan menjalankan fungsi dari objek functions
      fakeDb = {
        collection: function(col){
          return functions;
        }
      };
      // object functionsnya gak diimplementasi, biar di stub di tesing aja
      // karena kita maunya beda-beda callbackDb
      functions = {
        find: function(query, callbackDb) {}
      }
    });

    it('should return items from requester', function(){
      var result = {
        count : function(err,result){}
      };
      var dbStub = sinon.stub(db,'get').returns(fakeDb);
      var findStub = sinon.stub(functions,'find').yields(false,result);
      var resultStub = sinon.stub(result,'count').yields(false,{});
      var callback = sinon.spy();
      friendjs.getTotalRequest('azmi41',callback);
      callback.withArgs(false,{}).called.should.be.true;
      db.get.restore();
      functions.find.restore();
      result.count.restore();
    });

    it('should return error message if query error',function(){
      var dbStub = sinon.stub(db,'get').returns(fakeDb);
      var findStub = sinon.stub(functions,'find').yields(true,null);
      var callback = sinon.spy();
      friendjs.getTotalRequest('azmi41',callback);
      callback.withArgs(1,'not found').called.should.be.true;
      db.get.restore();
      functions.find.restore();
    });
  });
});


