var chai = require('chai');
var assert = chai.assert;
var should = chai.should();
var sinon = require('sinon');
var expect = require('chai').expect;
var facultyModel = require('../../models/faculty');
var db = require('../../db');

describe('get List Faculty', () => {
		var details;
		var callback;
		var callbackSpy;
		var obj;
		var toArray;

		beforeEach(function(){
			obj = {
			  toArray : function(err, items){

			  }
			};
			details = {
			  find : function(param){
			    return toArray;
			  }
			};
			faculty = {
			  collection : function(myDb){
			    return details;
			  }
			};
		callbackSpy = sinon.spy(callback);
		});



	describe('"listFaculty"', () => {
		it('should export a function', () => {
		  expect(facultyModel.getAll).to.be.a('function')
		})
	})

  	it('should pass object with correct values to save', function() {
		var stubGet = sinon.stub(db, 'get');
		var stubFind = sinon.stub(details, 'find');
		var stubData =  sinon.stub(obj,'toArray').yields(1,"Fakultas Ilmu Komputer");

		stubGet.returns(faculty);
		stubFind.returns(obj);
		facultyModel.getAll(callbackSpy);
		callbackSpy.withArgs(1,"Fakultas Ilmu Komputer").calledOnce.should.be.true;


		stubGet.restore();
		stubFind.restore();
		stubData.restore();
	});

  	
})