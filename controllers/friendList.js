var friends = require("../models/friend");

exports.viewFriendList = function(req,res){
	var count = 0;
	friends.getTotalRequest(req.params.username, function(err,obj){
	if(err){
		res.status("404");
		res.send("404: Page not found");
	}else{
	count = obj;
	friends.get(req.params.username, function(err,obj){
		if(obj === "empty"){
			res.render("friendList", { user: req.params.username, notif : count, title:"Friends List", friends: [] });
		}else{
			res.render("friendList", { user: req.params.username, notif : count, title:"Friends List", friends: obj.friends });
		}
	});
	}
});
};

exports.viewFriendRequest = function(req, res) {
	friends.getTotalRequest(req.params.username, function(err,obj){
	if(err){
		res.status("404");
		res.send("404: Page not found");
	}else{
	count = obj;
	friends.getRequest(req.params.username, function(err, items) {
		if(err) {
			res.status(err);
			res.send(items);
		}
		res.render('friendRequest', {user: req.params.username, notif : count, title:"Friend Request", request:items});
	});
	}
});
};

exports.acceptFriendRequest = function(req,res) {
	friends.deleteFriendRequest(req.params.username, req.params.other, function(err) {
		if(err) {
			res.status(500);
			res.send(err);
		}
		friends.addToFriend(req.params.username, req.params.other, function(err) {
			if(err) {
				res.status(500);
				res.send("500: "+err);
			}
			res.redirect('/profile/'+req.params.username+'/request');
		}); 
	});
}

exports.declineFriendRequest = function(req,res) {
	friends.deleteFriendRequest(req.params.username, req.params.other, function(err) {
		if(err) {
			res.status(500);
			res.send("500: "+err);
		}
		res.redirect('/profile/'+req.params.username+'/request');
	});
}

