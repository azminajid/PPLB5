var express = require('express');
var router = express.Router();
var multer  = require('multer');
var storage = multer.memoryStorage();
var upload = multer({ storage: storage });
var authentication = require('../middlewares/auth');

var home = require('./home');
var profile = require('./profile');

var search = require('./search');
var auth = require('./auth');
var friendList = require('./friendList');




// [GET] landing page
router.get('/', function(req,res){
    home.index(req,res);
});


router.get('/search',authentication.valid, function(req,res){
    search.getAllFormData(req,res);
});
router.post('/search',authentication.valid, function(req,res){
    search.searchFriends(req,res);
});

router.post('/addFriends',authentication.valid, function(req,res){
    res.send(req.body);
});


// [POST] sendrequest/(uesrname) to requestFriend as username
router.post('/requestFriend', authentication.valid, function(req,res,next) {
    search.updateRequest(req,res);
});


// [GET] view login
router.get('/login',function(req,res){
    auth.viewLogin(req,res);
});
// [GET] logout
router.get('/logout',function(req,res){
    auth.logout(req,res);
});
// [POST] submit authentication form
router.post('/login',function(req,res){
    auth.submitLogin(req,res);
});

// [GET] profile/{username} to view profile page
router.get('/profile/:username', function(req,res) {
    profile.viewProfile(req, res);
});
// [GET] profile/{username}/edit to view edit page
router.get('/profile/:username/edit', authentication.signed, function(req,res) {
    // no error in forms
    profile.viewEditProfile(req, res, []);
});
// [POST] profile/{username}/edit action for edit profile form
router.post('/profile/:username/edit', upload.single('photo'), function(req,res,next) {
    profile.updateProfile(req,res);
});

// [GET] profile/{username}/friend to view friend list
router.get('/profile/:username/friends', authentication.signed ,function(req,res,next) {

	friendList.viewFriendList(req,res);
});
// [GET] profile/{username}/request to view friend request
router.get('/profile/:username/request', authentication.signed ,function(req,res,next) {
    friendList.viewFriendRequest(req,res);
});
// [GET] to accept friend request
router.get('/:username/accept/:other', authentication.signed ,function(req,res,next) {
    friendList.acceptFriendRequest(req,res);
});
// [GET] to decline friend request
router.get('/:username/decline/:other', authentication.signed ,function(req,res,next) {
    friendList.declineFriendRequest(req,res);
});

router.get('*', function(req, res){
    res.status(404).send('404 Page Not Found');
});

module.exports = router
