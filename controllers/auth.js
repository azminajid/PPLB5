var user = require('../models/user');

// [GET] view login
exports.viewLogin = function (req,res) {
    res.render('login',{title:'umeeti'});
}

// [POST] submit authentication form
exports.submitLogin = function (req,res) {
    var username = req.body.username;
    user.get(username, function(err, result) {
        if(err) {
            res.redirect('/login');
        } else {
            req.session.authenticated = true;
            req.session.username = username;
            res.redirect('/profile/'+username);
        }
    });
}

exports.logout = function(req, res) {
    delete req.session.authenticated;
    delete req.session.username;
    res.redirect('/');
}