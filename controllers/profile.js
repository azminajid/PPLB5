var multer  = require('multer');
var storage = multer.memoryStorage();
var upload = multer({ storage: storage });
var Datauri = require('datauri');
var thisModule = require('./profile');

var array = require("../helpers/array");
var image = require("../helpers/image");

var user = require("../models/user");
var status = require("../models/status");
var interest = require("../models/interest");

// [GET] view user profile page
exports.viewProfile = function(req, res) {
  user.getWithStatus(req.params.username, function(err,obj) {
    if(err) {
      res.status(404);
      res.send("404 "+err);
    } else {
      res.render("profile", {
        title:"Profile",
        activeUser: req.session.username,
        user: obj.user,
        status: obj.status,
        auth: req.session.username == req.params.username,
        isValid: req.session.authenticated
      });
    }
  });
}

// [GET] view edit profile forms
exports.viewEditProfile = function(req, res, errors) {
  interest.getAll(function(err, obj){
    if(err) {
      res.send(err);
    }
    var interest_arr = obj;
    status.getAll(function(err, obj){
      if(err) {
        res.send(err);
      }
      var status_arr = obj;
      user.get(req.params.username, function(err,obj) {
        if(err) {
          res.send(err);
        } else {
          var this_user = obj.user;
          res.render("editProfile", {title:"Profile",
            username: this_user.username,
            user: this_user,
            errors: errors,
            status_arr: status_arr,
            interest_arr: interest_arr
          });
        }
      });
    });
  });
}

// [POST] update profile
exports.updateProfile = function(req,res) {
  var dataUri = null;
  if(req.file) {
    dataUri = new Datauri().format('.jpg', req.file.buffer).content;
  }
  req.assert('phone', 'phone harus diisi').notEmpty();
  req.assert('phone', 'phone harus angka saja').isNumeric();
  req.assert('email', 'email harus diisi').notEmpty();
  req.assert('location', 'location harus diisi').notEmpty();
  req.assert('facebook', 'link facebook harus diisi').notEmpty();
  req.assert('twitter', 'link twitter harus diisi').notEmpty();
  req.assert('linkein', 'link linkedin harus diisi').notEmpty();
  req.assert('facebook', 'link facebook harus dimulai dengan https://www.facebook.com/').isLink('www.facebook.com/');
  req.assert('twitter', 'link twitter harus dimulai dengan https://www.twitter.com/').isLink('www.twitter.com/');
  req.assert('linkein', 'link linkedin harus dimulai dengan https://www.linkedin.com/').isLink('www.linkedin.com/');
  req.getValidationResult().then(function(result) {
    var noValidationError = result.isEmpty();
    var withFile = req.file;
    var isFileImage = withFile && image.validation(req.file.mimetype);
    if(noValidationError && ( !withFile || (isFileImage))) {
      var userUpdate = req.body;
      userUpdate.skill = array.toArray(userUpdate.skill);
      userUpdate.achievement = array.toArray(userUpdate.achievement);
      userUpdate.photo = dataUri;
      userUpdate.interest = array.toArray(userUpdate.interest);
      user.update(req.params.username, userUpdate, function(err, result) {
        res.redirect("/profile/"+req.params.username);
      });
    } else {
      var error = result.array();
      if(req.file) error.push({param:"photo", msg:"file photo harus image", value:null});
      thisModule.viewEditProfile(req, res, error);
    }
  });
}
