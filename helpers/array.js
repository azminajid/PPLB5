
// change single object to array 
exports.toArray = function toArray(obj) {
    var isArray = obj instanceof Array;
    if(isArray) {
        return obj;
    } else if(obj) {
        return [obj];
    } else {
        return null;
    }
}