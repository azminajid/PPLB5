var fs = require("fs");
var mime = require("mime");
var util = require("util");

// validate image, is the file an image
exports.validation = function(mime_file) {
    var isMimeImage = mime_file == "image/jpeg" || mime_file == "image/png" || mime_file == "image/jpg";
    if(isMimeImage) {
        return true;
    } else {
        return false;
    }
}